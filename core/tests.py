from django.test import TestCase
from django.urls import reverse
from .models import *
from uuid import uuid4
from http.cookies import SimpleCookie
import datetime
import pytz
import json

#Models: Exercise, TopGym_User, WorkoutRoutine, WorkoutExercise, CompletedWorkout, Set, User_WorkoutRoutine, Friends, UserStats, Question, Answer, UserSurvey


class ModelTest(TestCase):
    def test_friends(self):
        u1 = TopGym_User(email='test@email.com')
        u1.save()
        u2 = TopGym_User(email='test2@email.com')        
        u2.save()
        u3 = TopGym_User(email='test3@email.com')
        u3.save()
        #test u1 and u2 arent yet friends
        self.assertFalse(Friends.objects.filter(friend_1=u1, friend_2=u2).exists())

        #make u1 and u2 friends
        friend1 = Friends(friend_1=u1, friend_2=u2)
        friend1.save()

        #test that u1 and u2 are now friends
        self.assertTrue(Friends.objects.filter(friend_1=u1, friend_2=u2).exists())

        #test that u1 and u3 are not friends
        self.assertFalse(Friends.objects.filter(friend_1=u1, friend_2=u3).exists())

        #make u1 and u3 friends
        friend2 = Friends(friend_1=u1, friend_2=u3)
        friend2.save()

        #test that u1 and u3 are friends
        self.assertTrue(Friends.objects.filter(friend_1=u1, friend_2=u3).exists())

        #test that u2 and u3 are not friends
        self.assertFalse(Friends.objects.filter(friend_1=u2, friend_2=u3).exists())

    def test_workout_routine(self):
        exercise1 = Exercise(name='Exercise 1', description='Exercise description', video_link='http://example.com')
        exercise1.save()
        exercise2 = Exercise(name='Push-ups', description='Push-ups description', video_link='https://www.youtube.com/watch?v=_l3ySVKYVJ8')
        exercise2.save()
        exercise3 = Exercise(name='Sit-ups', description='Sit-ups description', video_link='http://example.com')
        exercise3.save()

        u1 = TopGym_User(email='test@email.com')
        u1.save()
        u2 = TopGym_User(email='test2@email.com')        
        u2.save()
        
        #workout_routine made by u1, has exercise 1 & 2
        workout_routine = WorkoutRoutine(workout_id=1, created_by=u1, name='Workout 1', description='Workout description')#, total_completions=
        workout_routine.save()
        workout_exercise = WorkoutExercise(exercise_id=exercise1, workout_id=workout_routine, exercise_order=1)
        workout_exercise.save()
        workout_exercise2 = WorkoutExercise(exercise_id=exercise2, workout_id=workout_routine, exercise_order=2)
        workout_exercise2.save()

        #workout_routine2 made by u2, has exercise 3
        workout_routine2 = WorkoutRoutine(workout_id=2, created_by=u2, name='Workout 2', description='Example description')#, total_completions=
        workout_routine2.save()
        workout_exercise3 = WorkoutExercise(exercise_id=exercise3, workout_id=workout_routine2, exercise_order=1)        
        workout_exercise3.save()

        #workout_routine by u1 is completed
        completed_workout = CompletedWorkout(user=u1, workout_id=workout_routine)
        completed_workout.save()
        set = Set(set_id=1, CompletedWorkout_id=completed_workout, Exercise_id=exercise1, reps=10, weightage=20)
        set.save()
        set2 = Set(set_id=2, CompletedWorkout_id=completed_workout, Exercise_id=exercise2, reps=15, weightage=15)
        set2.save()


        #subscribe u1 to the workout_routine that it made
        user_workout_routine = User_WorkoutRoutine(user_id=u1, workout_routine_id=workout_routine)
        user_workout_routine.save()

        #subscribe u2 to workout_routine2 that it made
        user_workout_routine2 = User_WorkoutRoutine(user_id=u2, workout_routine_id=workout_routine2)
        user_workout_routine2.save()

        #subscribe u2 to u1's workout_routine
        user_workout_routine3 = User_WorkoutRoutine(user_id=u2, workout_routine_id=workout_routine)
        user_workout_routine3.save()


        #test that u1 is subscribed the workout_routine
        self.assertTrue(User_WorkoutRoutine.objects.filter(user_id=u1, workout_routine_id=workout_routine).exists())

        #test that u2 is subscribed the workout_routine
        self.assertTrue(User_WorkoutRoutine.objects.filter(user_id=u2, workout_routine_id=workout_routine).exists())

        #test that u2 is subscribed the workout_routine2
        self.assertTrue(User_WorkoutRoutine.objects.filter(user_id=u2, workout_routine_id=workout_routine2).exists())

        #test that u1 is not subscribed the workout_routine2
        self.assertFalse(User_WorkoutRoutine.objects.filter(user_id=u1, workout_routine_id=workout_routine2).exists())


        #test that exercise1 is in workout_routine
        self.assertTrue(WorkoutExercise.objects.filter(exercise_id=exercise1, workout_id=workout_routine).exists())

        #test that exercise2 is in workout_routine
        self.assertTrue(WorkoutExercise.objects.filter(exercise_id=exercise2, workout_id=workout_routine).exists())

        #test that exercise3 is in workout_routine2
        self.assertTrue(WorkoutExercise.objects.filter(exercise_id=exercise3, workout_id=workout_routine2).exists())

        #test that exercise3 is not in workout_routine
        self.assertFalse(WorkoutExercise.objects.filter(exercise_id=exercise3, workout_id=workout_routine).exists())


        #test that workout_routine is completed
        self.assertTrue(CompletedWorkout.objects.filter(workout_id=workout_routine).exists())

        #test that workout_routine2 is not completed
        self.assertFalse(CompletedWorkout.objects.filter(workout_id=workout_routine2).exists())

    #     friend = Friends(friend_1=u1, friend_2=u2)
    #     friend.save()
    #     user_stats = UserStats(user=u1, weight_lifted=100.5, consecutive_weeks=3, total_points=50)
    #     user_stats.save()
    #     question = Question(question_id=1, question='Random question?')
    #     question.save()
    #     answer = Answer(user=u1, question=question, answer='Example answer')
    #     answer.save()
    #     user_survey = UserSurvey(user=u1)
    #     user_survey.save()



class POST_tests(TestCase):
    def setUp(self):
        self.token = str(uuid4())
        self.email = "test@gmail.com"
        utc=pytz.UTC
        token_expire = utc.localize(datetime.datetime.now()) + datetime.timedelta(hours=1)
        TopGym_User.objects.update_or_create(email=self.email,token=self.token,token_expire=token_expire)

        self.client.cookies = SimpleCookie({"login_token":self.token})
    
    def test_authenticate(self):
        #test with valid token
        response = self.client.post("/authenticate",{"login_token":self.token})
        response_content = json.loads(response.content)
        print(response_content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_content["valid"],True)
        self.assertEqual(response_content["data"]["email"],self.email)

        #test with invalid token
        response = self.client.post("/authenticate",{"login_token":"abcd"})
        response_content = json.loads(response.content)
        print(response_content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_content["valid"],False)

#check for no redirects
class logged_in_tests(TestCase):
    def setUp(self):
        #create a user and append to database
        self.token = str(uuid4())
        self.email = "test@gmail.com"
        utc=pytz.UTC
        token_expire = utc.localize(datetime.datetime.now()) + datetime.timedelta(hours=1)
        self.user = TopGym_User.objects.update_or_create(email=self.email,token=self.token,token_expire=token_expire)[0]
        UserStats.objects.create(total_points = 1000,user =self.user)

        #create cookie using user login token
        self.client.cookies = SimpleCookie({"login_token":self.token})

        #create exercises
        exercise = Exercise(name="placeholder1",description="placeholder2",category="LEGS_BM")
        exercise.save()
        routine = WorkoutRoutine(created_by=self.user,name="placeholder3",description="placeholder4")
        routine.save()
        routine.exercises.add(exercise,exercise,exercise,exercise)


    def test_routines(self):
        response = self.client.get(reverse('routines'))
        self.assertEqual(response.status_code, 200)
    def test_make_routine(self):
        response = self.client.get(reverse('make-routine'))
        self.assertEqual(response.status_code, 200)
    def test_social(self):
        response = self.client.get(reverse('social'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'friends')
    def test_profile(self):
        response = self.client.get(reverse('profile'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.email)
    def test_survey(self):
        response = self.client.get(reverse('survey'))
        self.assertEqual(response.status_code, 200)
    def test_exercises(self):
        response = self.client.get(reverse('exercises'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'placeholder1')
    def test_friends(self):
        response = self.client.get(reverse('friends'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'add friend')
    def test_leaderboard(self):
        response = self.client.get(reverse('leaderboard'))
        self.assertEqual(response.status_code, 200)


#check for redirects
class logged_out_tests(TestCase):
    def setUp(self):
        self.client.cookies = SimpleCookie({"login_token":"abcd"})

    def test_routines(self):
        response = self.client.get(reverse('routines'))
        self.assertEqual(response.status_code, 302)
    def test_make_routine(self):
        response = self.client.get(reverse('make-routine'))
        self.assertEqual(response.status_code, 302)
    def test_social(self):
        response = self.client.get(reverse('social'))
        self.assertEqual(response.status_code, 302)
    def test_profile(self):
        response = self.client.get(reverse('profile'))
        self.assertEqual(response.status_code, 302)
    def test_survey(self):
        response = self.client.get(reverse('survey'))
        self.assertEqual(response.status_code, 302)
    def test_exercises(self):
        response = self.client.get(reverse('exercises'))
        self.assertEqual(response.status_code, 302)
    def test_friends(self):
        response = self.client.get(reverse('friends'))
        self.assertEqual(response.status_code, 302)
    def test_leaderboard(self):
        response = self.client.get(reverse('leaderboard'))
        self.assertEqual(response.status_code, 302)





    



