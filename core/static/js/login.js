
let logged_in = false;
let user_details = {};

async function login(token){
    var url_location = window.location.origin+"/authenticate";
    var csrf_token =  Cookies.get("csrftoken");
    var response = await $.ajax({
        url: url_location,
        type: "POST",
        data : {csrfmiddlewaretoken:csrf_token, login_token:token},
        dataType : "json",
        success: function (result) {
            console.log(result);
            console.log("logged_in!")

            logged_in = true;
            user_details.email = result.data.email;
            return result
        }
    });
    return response;
}

function logout(){
    Cookies.remove("login_token");
    let logged_in = false;
    let user_details = {};
}

$(document).ready(()=>{
    let token = Cookies.get('login_token');
    if (token != undefined){
        login(token)
    }
})