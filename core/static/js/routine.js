var sets = {};

function validateReps(reps) {
    return reps >= 1 && reps <= 1000;
}

function validateWeight(weight) {
    return weight >= 0 && weight <= 2000;
}

function submitRoutine() {
    // Get all sets
    let response_sets = [];
    let set_keys = Object.keys(sets);
    for (let i = 0; i < set_keys.length; i++) {
        response_sets[i] = [];
        for (let j = 0; j < sets["exercise_" + i].length; j++) {
            let reps = $("#exercise_" + i + "_set_" + j + "_reps").val();
            let weight = $("#exercise_" + i + "_set_" + j + "_weight").val();
            
            if (!validateReps(reps)) {
                alert("Invalid reps value for Exercise " + (i + 1) + ", Set " + (j + 1) + ". Reps must be between 1 and 1000.");
                return; // Stop form submission
            }
            
            if (!validateWeight(weight)) {
                alert("Invalid weight value for Exercise " + (i + 1) + ", Set " + (j + 1) + ". Weight must be between 0 and 2000.");
                return; // Stop form submission
            }
            
            response_sets[i].push({
                reps: reps,
                weight: weight
            });
        }
    }

    var routine_data = JSON.parse(JSON.parse(document.getElementById("routine_data").textContent))[0];
    console.log(routine_data);

    $.ajax({
        url: "/run-routine",
        type: "POST",
        data : {csrfmiddlewaretoken: Cookies.get("csrftoken"), routineID: routine_data.pk, exercises: response_sets},
        success: function (response) {
            window.location.replace("/routines");
        }
    });

}



$(document).ready(function () {
    // Double parse because double serialised by Django
    var routine_data = JSON.parse(JSON.parse(document.getElementById("routine_data").textContent))[0];
    var exercises_data = JSON.parse(JSON.parse(document.getElementById("exercises_data").textContent));

    $("#routine-name").text(routine_data.fields.name);
    $("#routine-desc").text(routine_data.fields.description);

    document.title = "TopGym - " + routine_data.fields.name;

    for (let i = 0; i < exercises_data.length; i++) {
        let id = "exercise_" + i;
        sets[id] = [];
        let exercise = $("<div>").attr("id", id);
        let title = $("<h3>").text(exercises_data[i].fields.name);
        title.addClass("exercise__title");
        let desc = $("<p>").text(exercises_data[i].fields.description);
        desc.addClass("exercise__desc");
        let sets_container = $("<div>").attr("id", id + "_sets");
        let addSet_btn = $("<button>").text("Add Set");
        addSet_btn.attr("id", id + "_btn")
        $(addSet_btn).on("click", function() {
            let set_id = id + "_set_" + sets[id].length
            let set_container = $("<div>").attr("id", set_id).addClass("set-container");
            let set_num = $("<h4>").text("Set " + (sets[id].length + 1));
            let reps = $("<input type='number' value=1 />").attr("id", set_id + "_reps").addClass("reps-input");
            let reps_label = $("<label>").attr("for", set_id + "_reps").text("reps");
            let weight = $("<input type='number' value=20 />").attr("id", set_id + "_weight").addClass("weight-input");
            let weight_label = $("<label>").attr("for", set_id + "_weight").text("kg");
            set_container.append(set_num);
            set_container.append(reps);
            set_container.append(reps_label);
            set_container.append(weight);
            set_container.append(weight_label);
            sets[id].push(set_container);
            $("#" + id + "_sets").append(set_container);
        });
        exercise.append(title);
        exercise.append(desc);
        exercise.append(sets_container);
        exercise.append(addSet_btn);
        $("#exercises").append(exercise);
        // Add an initial set
        $(addSet_btn).trigger("click");
    }
});
