//called when google signin succeeds
window.handleCredentialResponse = (response) => {
    //parse google credentials
    var csrf_token =  document.getElementsByName('csrfmiddlewaretoken')[0].value;

    // Create a form to send the response data to the backend
    let tokenForm = document.createElement('form');
    $(tokenForm).css("display", "none");
    $(tokenForm).attr("method", "POST");
    $(tokenForm).attr("action", "/login");

    let csrfmiddlewaretoken = $("<input>");
    csrfmiddlewaretoken.attr("type", "text");
    csrfmiddlewaretoken.attr("name", "csrfmiddlewaretoken");
    csrfmiddlewaretoken.val(csrf_token);
    $(tokenForm).append(csrfmiddlewaretoken);

    let jwt = $("<input>");
    jwt.attr("type", "text");
    jwt.attr("name", "jwt");
    jwt.val(response.credential);
    $(tokenForm).append(jwt);

    document.body.append(tokenForm);
    $(tokenForm).submit()
}
