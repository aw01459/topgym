from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='home'),
    path("login", views.login, name="login"),
    path("authenticate", views.authenticate, name="authenticate"),
    path("exercises", views.exercises, name ="exercises"),
    path("routine/<int:routine_id>/", views.routine, name = 'routine'),
    path("routines", views.routines, name = "routines"),
    path("make-routine",views.make_routine, name = "make-routine"),
    path("logout",views.logout,name="logout"),
    path("survey",views.survey,name="survey"),
    path("profile",views.profile,name="profile"),
    path("friends", views.friends, name ="friends"),
    path("leaderboard", views.leaderboard_page, name ="leaderboard"),
    path('remove_friend/<str:friend_email>/', views.remove_friend, name='remove_friend'),
    path("run-routine", views.run_routine, name= 'run-routine'),
    path("routines/<int:routine_id>/completed/", views.completed_routines, name = 'completed_routines'),
    path("map", views.map, name="map")
]
