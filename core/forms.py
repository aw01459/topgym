from django import forms
from django.core.exceptions import ValidationError
from django.forms import formset_factory
from .models import *

class RegistrationForm(forms.Form):

    firstName = forms.CharField(required=True,label="First Name")
    lastName = forms.CharField(required=True,label="Last Name")
    email = forms.EmailField(required=True,label="Email")
    password = forms.CharField(required=True,label="Password",widget=forms.PasswordInput())
    confirmPassword = forms.CharField(required=True,label="Confirm password",widget=forms.PasswordInput())

    #password validation
    def clean_password(self):
        data = self.cleaned_data['password']
        
        #check for symbols
        symbol = False
        for s in "?!%&*~@#'.,":
            if s in data: symbol = True
        if not symbol:
            raise ValidationError("Password has no symbols from ?!%&*~@#'.,")
        
        #check for digits
        digit = False
        for d in "0123456789":
            if d in data: digit =True
        if not digit:
            raise ValidationError("Password has no digits")
        
        #check length isn't smaller than 9 characters
        if len(data)< 10:
            raise ValidationError("Password is smaller than 10 characters")
    
    #confirm password validation
    def clean_confirmPassword(self):
        passwords = (self.cleaned_data["password"],self.cleaned_data["confirmPassword"]) #retrieve passwords from form

        if passwords[0] != passwords[1]:
            raise ValidationError("Passwords do not match")

#form for making a users workoutroutine
#The form on the page will consist of multiple of these forms, one for each exercise the user wants to add
class RoutineForm(forms.Form):
    exercise = forms.ModelChoiceField(
        label = 'Exercise',
        queryset = Exercise.objects.all(),
        empty_label = 'Choose an Exercise from the list',
        to_field_name = 'name'
    )


WorkoutRoutine_Form = formset_factory(RoutineForm, extra=3)


class SetForm(forms.Form):
    pass