from django.shortcuts import render
from django.http import HttpResponse
import time
import random
from .forms import RegistrationForm
from .models import *
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.urls import reverse


from .forms import WorkoutRoutine_Form, RoutineForm

from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, get_object_or_404
from django.core import serializers
from uuid import uuid4
import datetime
import json
from django.core.exceptions import ObjectDoesNotExist
import pytz
from google.oauth2 import id_token
from google.auth.transport import requests


from django.core import serializers
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

#TODO: Change redirect if cookie check fails, redirect to login 


def fetch_routine_exercises(routine_id):
  return Exercise.objects.filter(workoutexercise__workout_id = routine_id).order_by('workoutexercise__exercise_order')

def fetch_routine_exercise_ids(routine_id):
  exercises= Exercise.objects.filter(workoutexercise__workout_id = routine_id).order_by('workoutexercise__exercise_order')
  exercise_ids = [exercise.id for exercise in exercises ]


#secret key for jwt
client_ID = "1040438776271-r8v9vfuavccj3nvic5ppgt5q16ks12v0.apps.googleusercontent.com"

# Create your views here.
def index(request):
  validation = validate_user2(request)
  set_cookie = None
  if(validation!=None):
    user = validation['user']
    set_cookie = validation['set-cookie']
  total_users = str(len(TopGym_User.objects.all()))
  total_routines = str(len(WorkoutRoutine.objects.all()))
  response = render(request,'index.html',context = {"total_users":total_users,"total_routines":total_routines})

  if(set_cookie!= None):
    response.set_cookie(key ='login_token', value = set_cookie[0], expires = set_cookie[1])
  
  return response


def logout(request):
  login_token = request.COOKIES.get('login_token')
  print(login_token)
  #if user is logged out then just redirect home
  if login_token == '' or login_token == None:
    return redirect('home')

  try:
    found = TopGym_User.objects.get(token = login_token)
    
  except TopGym_User.DoesNotExist:

    response = redirect('login')
    response.delete_cookie(key = 'login_token')
    return response


  #delete the cookie from the database
  user = TopGym_User.objects.get(token = login_token)
  print(user.token)

  user.token = 0
  user.save()

  response = redirect('home')
  response.delete_cookie('login_token')
  return response
  



#method that takes page response, validates cookie and then returns
def validate_user2(request):
  #first read in current cookie from request object
  login_token = request.COOKIES.get('login_token') 
  try:
    user = TopGym_User.objects.get(token=login_token)
    print('user found')
  except:
    print('user not found')
    return None
  
  if login_token == 0:
    return None
  #check if logging in users token is expired
  #grab users token from database
  user_token = user.token
  
  #check if passed in token is the same as the one in the database
  equal = login_token == user_token
  if(not(equal)):
    #log the user out
    redirect('home')
  
  #if they're equal, then check if the stored token has expired
  utc=pytz.UTC
  current_time = utc.localize(datetime.datetime.now())
  token_expire = user.token_expire
  token_has_expired = current_time>token_expire
  
  if(equal and token_has_expired):
    #need to reissue cookie 
    new_cookie = str(uuid4())
    new_token_expire = utc.localize(datetime.datetime.now()) + datetime.timedelta(days=30)
    user.token = new_cookie
    user.token_expire = new_token_expire
    user.save()
    new_token_expire = datetime.datetime.strftime(new_token_expire, "%a, %d-%b-%y %H:%M:%S GMT")
    setcookie = [new_cookie,new_token_expire]
  if(equal and not(token_has_expired)):
    #token does not need resubmitting
    setcookie = None
  print('returning the dictionary')
  return_dict = {'user':user, 'set-cookie': setcookie}
  return return_dict



def routine(request, routine_id):
  validation = validate_user2(request)
  if validation is None:
    return redirect('login')

  user = validation['user']
  set_cookie = validation['set-cookie']

  if request.method == 'POST' and request.POST.get('_method') == 'DELETE':
    print('WE ARE HERE')
    #fetch the routine using routine_id
    print(routine_id)
    response = redirect('home')
    routines = WorkoutRoutine.objects.filter(workout_id = routine_id, created_by=user)
    if not routines:
      return redirect('routines')
    
    routine = routines[0]
    routine.delete()
    response = redirect('routines')


  #block for showing a specific routine
  elif request.method == 'GET':
    #fetch the routine using routine_id
    routines = WorkoutRoutine.objects.filter(workout_id = routine_id, created_by=user)
    if not routines:
      return redirect('routines')
    
    routine = routines[0]
    serialized_routine = serializers.serialize("json", [routine])

    #now need to grab the exercises for that routine
    #use the relation between workoutroutine and Exercise for this
    exercises = fetch_routine_exercises(routine_id)
    serialized_exercises = serializers.serialize("json", exercises)
    context = {'routine':serialized_routine, 'exercises':serialized_exercises}

    response = render(request, 'routine.html', context)
    
    if(set_cookie!= None):
      response.set_cookie(key ='login_token', value = set_cookie[0], expires = set_cookie[1])
  return response


#A GET method to fetch all Routines for a specific user
def routines(request):
  #each time we login to the website, the token field in TopGym_User table is automatically updated
  #we have access to this token in the cookies so we can grab it and use that to select the correct user object
  validation = validate_user2(request)
  if validation is None:
    return redirect('login')

  user = validation['user']
  set_cookie = validation['set-cookie']


  #this gives us the email, which we can use to query the WorkoutRoutine table
  routines = WorkoutRoutine.objects.filter(created_by = user.email)
  context = {'routines':routines}
  # Put the routines inside a context object
  response = render(request, 'routines.html', context)

  if(set_cookie!= None):
    response.set_cookie(key ='login_token', value = set_cookie[0], expires = set_cookie[1])


  return response



#fetches all exercises from the Exercise table
def exercises(request):
  exercises = Exercise.objects.all()
  context = {'exercises': exercises}
  return render(request, 'exercises.html', context)


def completed_routines(request,routine_id):
  validation = validate_user2(request)
  if validation is None:
    return redirect('login')

  user = validation['user']
  set_cookie = validation['set-cookie']
  
  routine = get_object_or_404(WorkoutRoutine,workout_id = routine_id)
  completed_routines = CompletedWorkout.objects.filter(workout_id = routine).order_by("date_completed")
  #if there is 5 or more of them, return the 5 most recent
  if len(completed_routines) >= 5:
    completed_routines = completed_routines[:5]

  context = {'routine':routine, 'completed_routines':completed_routines}
  
  response = render(request,'completed_workouts.html', context = context)
  if set_cookie != None:
    response.set_cookie(key ='login_token', value = set_cookie[0], expires = set_cookie[1])

  return response

def run_routine(request):
  validation = validate_user2(request)
  if validation is None:
    return redirect('login')

  user = validation['user']
  set_cookie = validation['set-cookie']

  if request.method == 'POST':
    data = request.POST.dict()
    routine_id = data['routineID']
    routine = WorkoutRoutine.objects.get(workout_id = routine_id)
    exercise_sets = [[key,value] for key,value in data.items()]
    exercise_sets = exercise_sets[2:]
    userStats_object = UserStats.objects.get(user=user)

    exercises =fetch_routine_exercises(routine_id)
    #make new CompletedWorkout object
    ran_workout = CompletedWorkout(user = user, workout_id = routine)
    ran_workout.save()
    for i in range(0, len(exercise_sets), 2):
      exercise = exercise_sets[i][0].split('[')[1]
      exercise = int(exercise.strip(']'))
      exercise_object = exercises[exercise]
      rep_count = int(exercise_sets[i][1])
      weightage = int(exercise_sets[i+1][1])
      print(exercise_object, ' ', rep_count, ' ', weightage)
      #insert new set for this exercise and this set
      new_set = Set(CompletedWorkout_id = ran_workout, Exercise_id = exercise_object, reps = rep_count, weightage=weightage)
      #increment userStats object weightage by the weight of the set
      userStats_object.weight_lifted = userStats_object.weight_lifted + rep_count*weightage

      new_set.save()

    #update the userStats object to have one more point (for another workout)
    userStats_object.total_points = userStats_object.total_points + 1 
    #save the userStats object
    userStats_object.save()
    leaderboard(request)

    response = HttpResponse(status=200)
    if(set_cookie!= None):
      response.set_cookie(key='login_token', value=set_cookie[0], expires=set_cookie[1])

    return response



def make_routine(request):
  validation = validate_user2(request)
  if validation is None:
    return redirect('login')

  user = validation['user']
  set_cookie = validation['set-cookie']

  if request.method == 'GET':
    formset = WorkoutRoutine_Form()
    # Calls the suggested_workout function which returns an array holding the suggested exercises for each muscle group
    response = render(request, 'make-routine.html', context = {'formset': formset, "suggestion" : suggestion_algorithm(user)})
    #response = render(request, 'make-routine.html', context = {'formset': formset})
    

  elif request.method == 'POST':
    print(request.POST.dict())
    data = list(request.POST.values())
    print(data)
    form_count = data[1]
    routine_name = data[5]
    routine_description = data[6]
    print("There are ",form_count, "exercises in this routine")
    formset = WorkoutRoutine_Form(request.POST, prefix='form')
    #handle the form entries

    if formset.is_valid():
      print("formset is valid")
      #if form is valid then make a new workout_routine, then fill in workout_exercise entries for each exercise in the routine
      new_routine = WorkoutRoutine(created_by=user, name = routine_name, description= routine_description)
      new_routine.save()
      for index, formdata in enumerate(formset):
        exercise_object = formdata.cleaned_data.get('exercise')
        print(exercise_object)
        try:
          exercise = Exercise.objects.get(name = exercise_object.name)
        except Exercise.DoesNotExist:
          print(f"Exercise '{exercise_object.name}' does not exist")
        workout_exercise = WorkoutExercise(exercise_id=exercise, workout_id=new_routine, exercise_order = index+1)
        workout_exercise.save()
    else:
      print(formset.errors)

    response =  redirect('routines')

  if(set_cookie!= None):
    response.set_cookie(key='login_token', value=set_cookie[0], expires=set_cookie[1])

  return response




def login(request):
    if request.method == "POST":
        JWT_token = request.POST.dict()["jwt"]
        time.sleep(0.1)
        credentials = id_token.verify_oauth2_token(JWT_token, requests.Request(), client_ID)
        email = credentials["email"]
        picture = credentials["picture"]
        first_name = credentials["given_name"]
        last_name = credentials["family_name"]
        print(credentials)
        token = str(uuid4())

        # Use utc to localize time zone
        utc=pytz.UTC
        token_expire = utc.localize(datetime.datetime.now()) + datetime.timedelta(hours=1)
        
        email_present = len(TopGym_User.objects.filter(email=email))>0
        
        if email_present:
            user = TopGym_User.objects.filter(email=email)[0]
            user.token = token
            user.token_expire = token_expire
            user.save()
        else:
            TopGym_User.objects.update_or_create(email=email,token=token,token_expire=token_expire,profile_picture_url=picture,first_name=first_name,last_name=last_name)
            user2 = TopGym_User.objects.filter(email=email)[0]
            print("test3333",user2)
            UserStats.objects.update_or_create(user=user2)
            print(UserStats.objects.get(user=user2).weight_lifted)
            print(UserStats.objects.get(user=user2).consecutive_weeks)
            print(UserStats.objects.get(user=user2).total_points)

        # Check if the user has completed the survey
        if UserSurvey.objects.filter(user_id=email).count() > 0:
           # User has completed the survey
           # TODO: This should probably redirect to to profile page or explore page
           print("User has completed survey")
           response = redirect('profile')
        else:
           # User has not completed the survey, make them
           # TODO: Redirect to the user survey
           print("User has not completed survey")
           response = redirect('survey')
        
        cookie_expire = utc.localize(datetime.datetime.now()) + datetime.timedelta(days=30)
        cookie_expire = datetime.datetime.strftime(cookie_expire, "%a, %d-%b-%y %H:%M:%S GMT")

        response.set_cookie("login_token", token, expires=cookie_expire)
        return response

    context = {}
    return render(request, 'login.html', context)


#url that can get a user's details with the correct token
def authenticate(request):
    if request.method == "POST":

        #check for token in request
        request_data = request.POST.dict()
        response_data = {"valid":False,"data":None}
        print(request_data)
        if "login_token" in request_data:
            token = request_data["login_token"]
            token_exists = len(TopGym_User.objects.filter(token=token))>0
            print("token in request")
            if token_exists:
                utc=pytz.UTC
                current_time = utc.localize(datetime.datetime.now())
                token_expire = TopGym_User.objects.filter(token=token)[0].token_expire
                token_has_expired = current_time>token_expire
                print("token exists")
                if not token_has_expired:
                    print("token has not expired",token)
                    email = TopGym_User.objects.filter(token=token)[0].email

                    response_data["valid"] = True
                    response_data["data"] = {"email":email}

        return HttpResponse(json.dumps(response_data), content_type="application/json")


def survey(request):
  # This if statement checks to see if there are any login cookies, and redirects the user to the registration page if not.
  if request.COOKIES.get('login_token') == None:
    print('User not logged in')
    return redirect('login')
  # This if statement takes the user's answers to the survey and inputs them into the database.
  if request.method == "POST":
    login_token = request.COOKIES.get('login_token')
    # Overwrites the user's previous survey answers with their new ones
    Answer.objects.filter(user=TopGym_User.objects.filter(token=login_token)[0]).delete()
    user = TopGym_User.objects.get(token = login_token)
    Answer.objects.update_or_create(user=user, question=Question.objects.get(question_id = 0), answer=request.POST.get('age'))
    Answer.objects.update_or_create(user=user, question=Question.objects.get(question_id = 1), answer=request.POST.get('equipment'))
    Answer.objects.update_or_create(user=user, question=Question.objects.get(question_id = 2), answer=request.POST.get('frequency'))
    Answer.objects.update_or_create(user=user, question=Question.objects.get(question_id = 3), answer=request.POST.get('rating'))
    Answer.objects.update_or_create(user=user, question=Question.objects.get(question_id = 4), answer=request.POST.get('goals'))
    print('Successfully wrote survey answers to database')
    return redirect('profile')
  else:
    return render(request, 'survey.html')
  
# call function to return list of friend emails (unique primary key)
def get_friends(user):
  # find friends assiociated with user
  friends = (Friends.objects.filter(friend_1=user) | Friends.objects.filter(friend_2 = user)).values('friend_1', 'friend_2')

  # initialise friends list
  friends_list = []
  # friends list get populated with friend emails
  for friend in friends:
    if friend['friend_1'] == user.email:
      friends_list.append(friend['friend_2'])
    else:
      friends_list.append(friend['friend_1'])
     
  #print(user, friends_list)
  # return list of friend emails
  return friends_list

def friends(request):
    if request.method == "POST":
      # call function to websocket
      leaderboard(request)
      error_message = None
      # Get user through token
      validation = validate_user2(request)
      if validation is None:
        return redirect('login')

      user = validation['user']
      set_cookie = validation['set-cookie']
      if request.method == "POST":
          # Recieves post message and queries for it
          friend_email = request.POST.dict()['friend_email']
          friends = [""]
          if friend_email != '':
              friend = TopGym_User.objects.filter(email=friend_email)
              if friend:
                  friend = friend[0]
                  if friend != user:
                    friend_qs = ((Friends.objects.filter(friend_1=user) & Friends.objects.filter(friend_2 = friend)) | 
                                (Friends.objects.filter(friend_1=friend) & Friends.objects.filter(friend_2 = user))).first()
                    if friend_qs:
                      error_message = "Already your friend"
                    else:
                      # Updates database, add user and other as friends
                      Friends.objects.update_or_create(friend_1=user,friend_2=friend)
                      # Redirect back
                      return redirect('friends')
                  else:
                    error_message = "Can't add yourself as a friend"
              else:
                  error_message = "Friend not found"
          else:
            error_message = "Enter a friend"
      else:
          friends = TopGym_User.objects.all()
      context = {'friends': friends, 'error_message': error_message}
      response = render(request, 'friends.html', context)
      if(set_cookie != None):
        response.set_cookie(key='login_token', value=set_cookie[0], expires=set_cookie[1])
      return response
    else:
      # Get user through token
      validation = validate_user2(request)
      if validation is None:
        return redirect('login')

      user = validation['user']
      set_cookie = validation['set-cookie']
      # Call function to get friend emails
      friends = get_friends(user)
      context = {'friend_data': friends}
      response = render(request, 'friends.html', context)
      if(set_cookie!= None):
        response.set_cookie(key='login_token', value=set_cookie[0], expires=set_cookie[1])

      return response

def leaderboard_page(request):
  context = {}
  return render(request, 'leaderboard.html', context)

# remove user friend from database
def remove_friend(request, friend_email):
  validation = validate_user2(request)
  if validation is None:
    return redirect('login')

  user = validation['user']
  set_cookie = validation['set-cookie']
  friend = get_object_or_404(TopGym_User, email = friend_email)
  # checks both configurations of user and friend
  friend_qs = ((Friends.objects.filter(friend_1=user) & Friends.objects.filter(friend_2 = friend)) | 
              (Friends.objects.filter(friend_1=friend) & Friends.objects.filter(friend_2 = user))).first()
               
  # delete friend object
  if friend_qs:
     friend_qs.delete()
  response = redirect('friends')
  if set_cookie != None:
    response.set_cookie(key='login_token', value=set_cookie[0], expires=set_cookie[1])
  
  return response


# function to update all user friends leaderboard through websocket.
def leaderboard(request):
  validation = validate_user2(request)
  if validation is None:
    return redirect('login')

  user = validation['user']

  friends = get_friends(user)
  friends.append(user.email)
  for friend in friends:
    update_leaderboard(TopGym_User.objects.get(email=friend).token)
    print("test2")

  return 

# update individual user leaderboard to websocket
def update_leaderboard(cookie):
  # use cookie to get user
  user = TopGym_User.objects.get(token=cookie)

  friends = get_friends(user)

  friends.append(user.email)
  print(friends)

  friends_data = []
  for friend in friends:
    # get friend login token
    friend_user = TopGym_User.objects.get(email=friend)
    # get friend user stats
    #print("test",friend_user)
    friend_userstats = UserStats.objects.filter(user=friend_user)
    #print("test2", friend_userstats)
    json_data = serializers.serialize('json', friend_userstats)
    friends_data.append([json_data, friend_user.first_name])

  # update websocket
  channel_layer = get_channel_layer()
  async_to_sync(channel_layer.group_send)(
    cookie,
    {'type' : 'data_update', 'data' : json.dumps(friends_data)}
    )

  return




def profile(request):
  validation = validate_user2(request)
  if validation is None:
    return redirect('login')

  user = validation['user']
  set_cookie = validation['set-cookie']

  image = user.profile_picture_url
  name = user.first_name
  surname = user.last_name

  #UserStats.objects.update_or_create(user=user, weight_lifted = 1000.0, consecutive_weeks = 8, total_points = 2000)

  stats = UserStats.objects.get(user=user)

  context = {"email": user.email, "image" : image, "name" : name, "surname" : surname, "stats" : stats}
  response = render(request, 'profile.html', context)
  if(set_cookie!= None):
    response.set_cookie(key='login_token', value=set_cookie[0], expires=set_cookie[1])
  return response

  

# An algorithm that takes the user's answers to the survey and uses them to recommend exercises
def suggestion_algorithm(user):

  answer = []
  # For loop populates an array called 'answer[]' with the user's survey responses
  answers = Answer.objects.filter(user=user)

  for i in answers:

    answer.append(i.answer)
  
  intensity = 0.0
  cap = 1.0
  BuildMuscle = ''
  # Average intensity score over inf survey responses given an even distrubution is: 0.6885
  # Weighs the answers to the survey based on how likely the user is to be an intensive exerciser, 1 being high intensity, 0 being low intensity
  match answer[0]:
    case 'under15':
      cap = 0.75
    case '15to25':
      intensity = 1.0
    case '25to40':
      intensity = 0.75
    case '40to60':
      intensity = 0.5
    case 'over60':
      intensity = 0.25

  match answer[1]:
    case 'under18':
      intensity += 0.75
    case '19to24':
      intensity += 1.0
    case '25to29':
      intensity += 0.8
    case '30to39':
      intensity += 0.6
    case 'over40':
      intensity += 0.4

  match answer[2]:
    case 'every_day':
      intensity += 0.7
    case 'a_few_times_a_week':
      intensity += 1.0
    case 'once_a_week':
      intensity += 0.75
    case 'less_than_once_a_week':
      intensity += 0.5

  match answer[3]:
    case 'very_good':
      intensity += 1
    case 'good':
      intensity += 0.8
    case 'adequate':
      intensity += 0.6
    case 'subpar':
      intensity += 0.4
    case 'bad':
      intensity += 0.3
  
  match answer[4]:
    case 'aethstetics':
      intensity += 1
    case 'lose_weight':
      intensity += 0.75
    case 'be_healthy':
      intensity += 0.5

  intensity /= 5

  # Assigns the user a tag of IS (In Shape) or BM (Build Muscle) based on their likely fitness goals. BM exercises tend to be more intensive
  # The intensity is capped at 0.7 for users below the age of 15 as it is more likely to be harmful to workout intensly while your body is developing
  if intensity < 0.7 or cap < 0.7:
    BuildMuscle = 'IS'
  else:
    BuildMuscle = 'BM'

  # Iterates through body parts and suggests a random excercise that matches the assigned tag
  Body = ['CHEST', 'CORE', 'ARMS', 'LEGS']
  Suggestion = []
  for i in range(0,4):
    temp = Body[i] + '_' + BuildMuscle
    random_number = random.randint(0, len(Exercise.objects.filter(category = temp))-1 )

    Suggestion.append(Exercise.objects.filter(category = temp)[random_number].name)

  return Suggestion



def map(request):
  return render(request, 'map.html', context = {})