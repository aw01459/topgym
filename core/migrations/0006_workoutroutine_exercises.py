# Generated by Django 4.1.7 on 2023-05-16 22:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_alter_workoutroutine_workout_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='workoutroutine',
            name='exercises',
            field=models.ManyToManyField(through='core.WorkoutExercise', to='core.exercise'),
        ),
    ]
