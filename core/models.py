from django.db import models
from django.contrib.auth.hashers import make_password

from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _

#TODO: Could add user_id column to exercise table, default to null for pre-defined exercise, insert user_id for user defined exercises

#table for exercises, each record has name, description and video link
class Exercise(models.Model):
	class Exercise_Category(models.TextChoices):
		LEGS_BUILDMUSCLE = 'LEGS_BM', _('Legs Build Muscle')
		ARMS_BUILDMUSCLE = 'ARMS_BM', _('Arms Build Muscle')
		CHEST_BUILDMUSCLE = 'CHEST_BM', _('Chest Build Muscle')
		LEGS_INSHAPE = 'LEGS_IS', _('Legs In Shape')
		ARMS_INSHAPE = 'ARMS_IS', _('Arms In Shape')
		CHEST_INSHAPE = 'CHEST_IS', _('Chest In Shape'), 
		CORE_INSHAPE = 'CORE_IS', _('Core In Shape')
		CORE_BUILDMUSCLE = 'CORE_BM', _('Core Build Muscle')


	#has own auto incrementing ID
	name = models.CharField(max_length=50, unique = True)
	description = models.CharField(max_length=256, verbose_name="Description of exercise")
	category = models.CharField(
		max_length = 8,
		choices = Exercise_Category.choices,
		null=True
	)
  



class User(AbstractUser):
	pass

class TopGym_User(models.Model):
	email = models.EmailField(primary_key=True)
	token = models.CharField(max_length=36)
	token_expire = models.DateTimeField(default="0/0/0 00:00")
	profile_picture_url = models.URLField(max_length=200,default="")
	first_name = models.CharField(max_length=36,default="")
	last_name = models.CharField(max_length=36,default="")

#table for workout plans
class WorkoutRoutine(models.Model):
	workout_id = models.BigAutoField(primary_key=True)
	created_by = models.ForeignKey(TopGym_User, on_delete=models.DO_NOTHING, null=True)
	name = models.CharField(max_length = 30)
	description = models.CharField(max_length=50)
	total_completions = models.IntegerField(default=0)
	exercises = models.ManyToManyField(Exercise, through= 'WorkoutExercise')

#A join table that links a workout routine with an exercise
class WorkoutExercise(models.Model):
	#has own auto incrementing ID
	exercise_id = models.ForeignKey(Exercise, on_delete=models.CASCADE)
	workout_id = models.ForeignKey(WorkoutRoutine, on_delete = models.CASCADE)
	exercise_order = models.IntegerField()


#table for completed workouts, contains foreign key from User and workout_ID
class CompletedWorkout(models.Model):
	#has own auto incrementing ID
	user = models.ForeignKey(TopGym_User, on_delete = models.CASCADE)
	workout_id = models.ForeignKey(WorkoutRoutine, on_delete = models.CASCADE)
	date_completed = models.DateTimeField(auto_now_add=True)


#table for a set, belongs to completed_workout. Contains details such as reps, weightage and date completed
class Set(models.Model):
	set_id = models.BigAutoField(primary_key=True)
	CompletedWorkout_id = models.ForeignKey(CompletedWorkout,on_delete=models.CASCADE)
	Exercise_id = models.ForeignKey(Exercise, on_delete = models.DO_NOTHING)
	reps = models.IntegerField(default = 0)
	weightage = models.IntegerField(default = 0)




#table thats lets users 'subscribe' to workout routines, has FKs from User and WorkoutRoutine table
class User_WorkoutRoutine(models.Model):
	user_id = models.ForeignKey(TopGym_User, on_delete = models.DO_NOTHING)
	workout_routine_id = models.ForeignKey(WorkoutRoutine, on_delete = models.DO_NOTHING)



class Friends(models.Model):
	friend_1 = models.ForeignKey(TopGym_User, on_delete=models.CASCADE, related_name='friend_1')
	friend_2 = models.ForeignKey(TopGym_User, on_delete=models.CASCADE, related_name='friend_2')


class UserStats(models.Model):
	user = models.ForeignKey(TopGym_User, on_delete=models.CASCADE)
	weight_lifted = models.FloatField(default=0.0)
	consecutive_weeks = models.IntegerField(default=0)
	total_points = models.IntegerField(default=0)

class Question(models.Model):
	question_id = models.IntegerField(primary_key=True)
	question = models.TextField()


class Answer(models.Model):
	user = models.ForeignKey(TopGym_User, on_delete=models.CASCADE)
	question = models.ForeignKey(Question, on_delete=models.CASCADE)
	answer = models.TextField(default='')
	



#may need changing depending on format of survey
class UserSurvey(models.Model):
	user = models.ForeignKey(TopGym_User, on_delete=models.CASCADE)
	date = models.DateField(auto_now_add=True)


