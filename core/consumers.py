from channels.generic.websocket import WebsocketConsumer

import json
from random import randint

from asgiref.sync import async_to_sync

from .views import update_leaderboard

class WSConsumer(WebsocketConsumer):
    def connect(self):

        cookie = self.scope['cookies']['login_token']
        print(cookie)

        self.group = cookie

        async_to_sync(self.channel_layer.group_add)(
            self.group,
            self.channel_name
        )

        self.accept()

        update_leaderboard(cookie)
        #self.send(json.dumps({'message' : randint(1, 100)}))



    def data_update(self, event):
        data = event['data']  # Received serialized data
        self.send(data)  # Sending data to the connected WebSocket client(s)
