FROM python:3.11

# Don't buffer python output
ENV PYTHONUNBUFFERED 1

# Set working directory
WORKDIR /topgym

# Copy list of required Python modules
COPY requirements.txt /topgym/requirements.txt

# Install said required modules
RUN pip install -r requirements.txt

# Install 'mysql' command for viewing database
RUN apt update
RUN apt install mariadb-client -y
