# Steps for running the container
1. Create a '.env' file in the root directory of the repository (/topgym) <br />
2. In the .env file add the following two variables:
    1. MYSQL_DATABASE='topgym' <br />
    2. MYSQL_ROOT_PASSWORD='test123' <br />

3. Finally, in the console run the following command: <br />
**docker-compose up**

